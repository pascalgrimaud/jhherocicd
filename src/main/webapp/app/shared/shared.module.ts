import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { JhherocicdSharedLibsModule, JhherocicdSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [JhherocicdSharedLibsModule, JhherocicdSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [JhherocicdSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhherocicdSharedModule {
  static forRoot() {
    return {
      ngModule: JhherocicdSharedModule
    };
  }
}
